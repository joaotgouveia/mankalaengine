### For Debian Based Systems
#### Installing GCC Compiler
```sh
sudo apt install g++
```
#### Installing CMake
```sh
sudo apt install cmake
```
#### OpenGl drivers
```sh
sudo apt install libgl1 libglu1-mesa
```
#### Installing Extra CMake Modules (ECM)
```sh
sudo apt install extra-cmake-modules
```
#### Installing Qt
```sh
sudo apt install qt6-tools-dev qt6-tools-dev-tools
```
### For Fedora Based Systems
#### Installing GCC Compiler
```sh
sudo dnf install gcc-c++
```
#### Installing CMake
```sh
sudo dnf install cmake
```
#### Installing OpenGl drivers
```sh
sudo dnf install mesa-libGL mesa-libGLU
```
#### Installing Extra CMake Modules (ECM)
```sh
sudo dnf install extra-cmake-modules
```
#### Installing Qt
```sh
sudo dnf install qt6-qtbase qt6-qtbase-devel
```
