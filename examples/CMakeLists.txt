file(GLOB_RECURSE EXAMPLES *.cpp)
if(BUILD_EXAMPLES)
    foreach(example_src ${EXAMPLES})
        get_filename_component(example_name ${example_src} NAME_WE)

        add_executable(${example_name} ${example_src})
        target_link_libraries(${example_name} PRIVATE MankalaEngine)

        target_include_directories(${example_name} PUBLIC ../include/mankalaengine)
        ecm_mark_nongui_executable(${example_name})
    endforeach()
endif()
